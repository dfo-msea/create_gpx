# Create GPX file

__Main author:__  Sarah Davies  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Pacific Biological Station   
__Contact:__      e-mail: Sarah.Davies@dfo-mpo.gc.ca | tel: 250-756-7124


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)


## Objective
This repository contains two scripts used to convert either csv or shp files to gpx format to load onto OpenCPN navigation software.

## Summary
Prepare data locations to add to OpenCPN software on ToughBooks.

## Status
Completed

## Contents
**input_data folder**
* Sample data for running the code (sites.csv and sites.shp)
* Simple coastline for plotting, this coastline may not include some small islands

**output_data folder**
* Sample data for running the code (sites_WGS.gpx)

**writeCSV_to_GPX.R**
* reads in a csv file and writes a gpx file

**writeSHP_to_GPX.R**
* reads a shp file and writes a gpx file

## Methods
Files from the input_data folder are loaded into workspace.
Data format is checked for field names and projection.
If projection is not WGS, then data is reprojected.
Data is plotted as a visual check.
Data is exported into output_data folder as a gpx file.

## Requirements
1. Scripts assume the first column of input file contains unique identifier
2. Scripts assume the coordinates fields are named "lat" and "lon"
3. Edit file names listed under Controls, as needed




